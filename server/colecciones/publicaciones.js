Meteor.startup(function(){
	Meteor.publish('productos', function(){
		return Products.find();
	});

	Meteor.publish('categories', function(){
		return Categories.find();
	});

	Meteor.publish('subcategories', function(){
		return SubCategories.find();
	});

	Meteor.publish('preguntas', function(){
		return Preguntas.find();
	});

	Meteor.publish('paises', function(){
		return Paises.find();
	});

	Meteor.publish('ventas', function(){
		return Ventas.find();
	});

	Meteor.publish('imagenes', function(){
		return Imagenes.find();
	});

	Meteor.publish('usuarios', function(){
		return Meteor.users.find();
	});
});