  Router.route('registrarse', {
    path: '/registrarse',
    template: 'registrarse',
    waitOn: function() { 
      return Meteor.subscribe('usuarios'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('login', {
    path: '/login',
    template: 'login',
    waitOn: function() { 
      return Meteor.subscribe('usuarios'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('verificarEmail', {
    path: '/verificarEmail',
    template: 'verificarEmail',
    waitOn: function() { 
      return Meteor.subscribe('usuarios'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('introducir', {
    path: '/verificarEmail/introducir',
    template: 'introducir',
    waitOn: function() { 
      return Meteor.subscribe('usuarios'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });