Template.busqProducto.helpers({ 
	'Categories': function(){
		return Categories.find({});
	},
	'SubCategories': function(){
		return SubCategories.find({cat: this._id});
	}
});

Template.busqProducto.events({
	'click .collapsible-header': function(){
		$('.collapsible').collapsible({
		      accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
		    }); 
		return true;
	}
});