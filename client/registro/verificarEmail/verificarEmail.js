Template.verificarEmail.helpers({
	verificarEmail: function(){
		if(Meteor.user().emails[0].verified === true){
			Router.go('/');
			return false;
		}else{
			return true;
		}
	}
});

Template.verificarEmail.events({
	'click #verificar': function(){
		Meteor.call('verificarEmail', chance.guid(), Meteor.user().emails[0].address, Meteor.user().profile.username, function(err, data){
			if(err){
				return noty({
                      position: 'topRight',
                      text: 'Ocurrio un Error',
                      animation: {
                          open: {height: 'toggle'}, // jQuery animate function property object
                          close: {height: 'toggle'}, // jQuery animate function property object
                          easing: 'swing', // easing
                          speed: 500 // opening & closing animation speed
                      }
                  });
			}else{
				Session.set('codigoVerificacion', data);
				Router.go('/verificarEmail/introducir');
				return true;
			}
		});
		return true;
	}
})