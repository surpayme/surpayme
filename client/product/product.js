Template.product.helpers({
  productoID: function(){
    return Products.find({_id: Session.get('productID')});
  },

  comments: function(){
    return Preguntas.find({ productID: Session.get('productID')});
  },

  producto: function(){
    return Products.find({_id: Session.get('productID')});
  },

  priceTo: function(){

      switch(this.price.divisa){
        case "VEF":
          var priceTo = this.price.cantidad / Session.get('pricesBTC').BTC.VEF;
          Session.set('priceProduct', priceTo);
          return priceTo.toFixed(8);
          break;

        case "USD":
          var priceTo = this.price.cantidad / Session.get('pricesBTC').BTC.USD;
          Session.set('priceProduct', priceTo);
          return priceTo.toFixed(8);
          break;

        case "ARS":
          var priceTo = this.price.cantidad / Session.get('pricesBTC').BTC.ARS;
          Session.set('priceProduct', priceTo);
          return priceTo.toFixed(8);
          break;
      }
      Session.set('priceProduct', priceTo);
      return priceTo.toFixed(8);
  }
});

Template.product.events({
	"click .comprar": function(){
    Session.set('userSeller', Products.findOne({ _id: Session.get('productID')}).sellerData);
		
    return Session.set('productoCompra', Session.get('productID'));
	}
});