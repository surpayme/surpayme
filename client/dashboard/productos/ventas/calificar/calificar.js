Template.calificar.events({
	"click #calificar": function(){
		Meteor.call('calificarVenta', this._id, this.nombre, this.link, this.monto, this.wallet, this.linkTransaccion, this.userComprador, this.userSeller, this.fotoEnvio, $('#calificacion').val(), function(err){
			if(err){
				console.log(err);
				return noty({
			          position: 'topRight',
			          text: err,
			          animation: {
			              open: {height: 'toggle'}, // jQuery animate function property object
			              close: {height: 'toggle'}, // jQuery animate function property object
			              easing: 'swing', // easing
			              speed: 500 // opening & closing animation speed
			          }
			      });
			}else{
				return noty({
			          position: 'topRight',
			          text: 'La Calificación de la venta ya esta registrado en el panel del usuario',
			          animation: {
			              open: {height: 'toggle'}, // jQuery animate function property object
			              close: {height: 'toggle'}, // jQuery animate function property object
			              easing: 'swing', // easing
			              speed: 500 // opening & closing animation speed
			          }
			      });
			}
		});
	}
});