Template.ventas.helpers({
	ventasProceso: function() {
		return Ventas.find({ userSeller: Meteor.user(), calificado: false});
	},
	enviado: function(){
		if(Ventas.findOne({_id: this._id, userComprador: Meteor.user(), enviado: true})){
			return true;
		}else{
			return false;
		}
	},
	emailComprador: function(){
		return this.userComprador.emails[0].address;
	}
});

Template.ventas.events({
	'change #estadoEnvio': function(){
		$('#textSw').removeClass('red-text');
		$('#textSw').addClass('green-text');
		$('#estadoEnvio').attr('disabled', true);

		Meteor.call('ventasUpdate', this._id, this.nombre, this.link, this.monto, this.wallet, this.linkTransaccion, this.userComprador, this.userSeller, function(err){
			if(err){
				console.log(err);
				return noty({
			          position: 'topRight',
			          text: err,
			          animation: {
			              open: {height: 'toggle'}, // jQuery animate function property object
			              close: {height: 'toggle'}, // jQuery animate function property object
			              easing: 'swing', // easing
			              speed: 500 // opening & closing animation speed
			          }
			      });
			}else{
				return noty({
			          position: 'topRight',
			          text: 'Tu envio ya esta registrado en el panel del usuario',
			          animation: {
			              open: {height: 'toggle'}, // jQuery animate function property object
			              close: {height: 'toggle'}, // jQuery animate function property object
			              easing: 'swing', // easing
			              speed: 500 // opening & closing animation speed
			          }
			      });
			}
		});
		return true;
	},
	"click #subirVerificacion": function(){
		Session.set('waitingForApiResponse', true);
	      MeteorCamera.getPicture({
	        width: 800,
	        height: 600,
	        quality: 90
	      }, function (error, data) {
	        if (error) {
	          throw error;
	        } else {
	          Imgur.upload({
	            image: data,
	            apiKey: ServiceConfiguration.configurations.findOne({ service: "imgur"}).secretKey
	          }, function (error, data) {
	            Session.set('waitingForApiResponse', false);
	            if (error) {
	              throw error;
	            } else {
	              Session.set("photo", data.link);
	              Session.set("deleteHash", data.deletehash);
	              Meteor.call('ventasUpdate', this._id, this.nombre, this.link, this.monto, this.wallet, this.linkTransaccion, this.userComprador, this.userSeller, Session.get("photo"), function(err){
						if(err){
							console.log(err);
							return noty({
						          position: 'topRight',
						          text: err,
						          animation: {
						              open: {height: 'toggle'}, // jQuery animate function property object
						              close: {height: 'toggle'}, // jQuery animate function property object
						              easing: 'swing', // easing
						              speed: 500 // opening & closing animation speed
						          }
						      });
						}else{
							return noty({
						          position: 'topRight',
						          text: 'La foto de verificación de envio ya esta registrado en el panel del usuario',
						          animation: {
						              open: {height: 'toggle'}, // jQuery animate function property object
						              close: {height: 'toggle'}, // jQuery animate function property object
						              easing: 'swing', // easing
						              speed: 500 // opening & closing animation speed
						          }
						      });
						}
					});
	            }
	          });
	        }
	      });
		return true;
	}
});