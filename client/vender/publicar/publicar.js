Template.publicar.events({
  'click #publicarBtn': function(){
  	Meteor.call('productsInsert', Session.get('userInfo'), Session.get('name'), Session.get('desc'), Session.get('price'), Session.get('divisa'), Session.get('pais'), Session.set('cantidad', $('#cantidad').val()), Session.get('estado'), Session.get('ciudad'), Session.get('catName'), new Date(), Session.get('foto1'), Session.get('foto2'), Session.get('foto3'), Session.get('foto4'), function(err){
		if(err){
			Session.set('userInfo', null);
			Session.set('name', null);
			Session.set('desc', null);
			Session.set('price', null);
			Session.set('pais', null);
			Session.set('estado', null);
			Session.set('ciudad', null);
			Session.set('catName', null);
			Session.set('foto1', null);
			Session.set('foto2', null);
			Session.set('foto3', null);
			Session.set('foto4', null);
			return noty({
				        position: 'topRight',
				        text: err,
				        animation: {
				            open: {height: 'toggle'}, // jQuery animate function property object
				            close: {height: 'toggle'}, // jQuery animate function property object
				            easing: 'swing', // easing
				            speed: 500 // opening & closing animation speed
				        }
				    });
		}else{
			Session.set('userInfo', null);
			Session.set('name', null);
			Session.set('desc', null);
			Session.set('price', null);
			Session.set('pais', null);
			Session.set('estado', null);
			Session.set('ciudad', null);
			Session.set('catName', null);
			Session.set('foto1', null);
			Session.set('foto2', null);
			Session.set('foto3', null);
			Session.set('foto4', null);
			noty({
			        position: 'topRight',
			        text: 'Paso 2/2 listo, Tu producto ha sido publicado con exito!',
			        animation: {
			            open: {height: 'toggle'}, // jQuery animate function property object
			            close: {height: 'toggle'}, // jQuery animate function property object
			            easing: 'swing', // easing
			            speed: 500 // opening & closing animation speed
			        }
			    });
			return Router.go('/');
		}
	});
    return true;
  }
});
