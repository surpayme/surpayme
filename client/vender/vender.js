Template.vender.helpers({
  subCategories: function(){
    return SubCategories.find({});
  },
  paisOrigen: function(){
    return Paises.find({});
  }
});

Template.vender.events({

  'submit .publicar': function (e) {
    e.preventDefault();

    /*HTTP.call('GET', 'http://api.bitcoinvenezuela.com/', function(err, data){
        if(err){
            return err;
        }else{
            return Session.set('prices', data);
        }
    });

    var prices = Session.get('prices').content;

    prices = EJSON.parse(prices);*/

    Session.set('userInfo', Meteor.user());
    Session.set('name',    $('#nombre').val());
    Session.set('desc',    $('#descripcion').val());
    Session.set('divisa',  $('#divisa').val());
    Session.set('price',   $('#precio').val());
    Session.set('cantidad', $('#cantidad').val());
    Session.set('pais',    $('#pais').val());
    Session.set('estado',  $('#estado').val());
    Session.set('ciudad',  $('#ciudad').val());
    Session.set('catName', $('#catName').val());
    if(Session.get('name') && Session.get('desc') && Session.get('price') && Session.get('divisa') && Session.get('pais') && Session.get('cantidad') && Session.get('estado') && Session.get('ciudad') && Session.get('catName')){
        Router.go('/vender/subirFotos');
        noty({
            position: 'topRight',
            text: 'Paso 1/2 listo para publicar tu producto',
            animation: {
                open: {height: 'toggle'}, // jQuery animate function property object
                close: {height: 'toggle'}, // jQuery animate function property object
                easing: 'swing', // easing
                speed: 500 // opening  && closing animation speed
            }
        });
    }else{
        noty({
            position: 'topRight',
            text: 'Debes Llenar todos los campos del producto',
            animation: {
                open: {height: 'toggle'}, // jQuery animate function property object
                close: {height: 'toggle'}, // jQuery animate function property object
                easing: 'swing', // easing
                speed: 500 // opening & closing animation speed
            }
        });
    }
    return true;
  }
});
