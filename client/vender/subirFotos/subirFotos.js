Template.subirFotos.rendered = function(){
    var arrayOfImageIds = [];

    Dropzone.autoDiscover = false;

    Meteor.Dropzone.options.maxFiles = 4;

    if(Session.get('userInfo') && Session.get('name') && Session.get('desc') && Session.get('price') && Session.get('pais') && Session.get('estado') && Session.get('ciudad') && Session.get('catName')){
      var dropzone = new Dropzone("#dropzone", {
          accept: function(file, done){
              Imagenes.insert(file, function(err, fileObj){
                  if(err){
                    console.log(err);
                    return noty({
                        position: 'topRight',
                        text: err,
                        animation: {
                            open: {height: 'toggle'}, // jQuery animate function property object
                            close: {height: 'toggle'}, // jQuery animate function property object
                            easing: 'swing', // easing
                            speed: 500 // opening & closing animation speed
                        }
                    });
                  } else {
                    // gets the ID of the image that was uploaded
                    var imageId = fileObj._id;
                    // do something with this image ID, like save it somewhere
                    arrayOfImageIds.push(imageId);
                    Session.set('imagesID', arrayOfImageIds);

                    noty({
                        position: 'topRight',
                        text: 'Foto Subida Con exito',
                        animation: {
                            open: {height: 'toggle'}, // jQuery animate function property object
                            close: {height: 'toggle'}, // jQuery animate function property object
                            easing: 'swing', // easing
                            speed: 500 // opening & closing animation speed
                        }
                    });

                    if(Session.get('imagesID').length === 4){
                      var array = Session.get('imagesID');
                     
                      setTimeout(function(){
                        Session.set('foto1', 'http://localhost'+Imagenes.findOne({_id: array[0]}).url());
                      }, 1000);
                     
                      setTimeout(function(){
                        Session.set('foto2', 'http://localhost'+Imagenes.findOne({_id: array[1]}).url());
                      }, 2000);
                     
                      setTimeout(function(){
                        Session.set('foto3', 'http://localhost'+Imagenes.findOne({_id: array[2]}).url());
                      }, 3000);
                     
                      setTimeout(function(){
                        Session.set('foto4', 'http://localhost'+Imagenes.findOne({_id: array[3]}).url());
                        Router.go('/vender/publicar');
                      }, 4000);                    
                    }
                    return true;
                  };
              });
          }
      });
    }
};

Template.subirFotos.events({
  'click .capture1': function(){
    MeteorCamera.getPicture(function(err, data){
      if(err){
        return err;
      }else{
        Session.set('foto1', data);
        return true;
      }
    });
  },
  'click .capture2': function(){
    MeteorCamera.getPicture(function(err, data){
      if(err){
        return err;
      }else{
        Session.set('foto2', data);
        return true;
      }
    });
  },
  'click .capture3': function(){
    MeteorCamera.getPicture(function(err, data){
      if(err){
        return err;
      }else{
        Session.set('foto3', data);
        return true;
      }
    });
  },
  'click .capture4': function(){
    MeteorCamera.getPicture(function(err, data){
      if(err){
        return err;
      }else{
        Session.set('foto4', data);
        return true;
      }
    });
  }
});

Template.subirFotos.helpers({
  foto1: function(){
    return Session.get('foto1');
  },
  foto2: function(){
    return Session.get('foto2');
  },
  foto3: function(){
    return Session.get('foto3');
  },
  foto4: function(){
    return Session.get('foto4');
  },
  mobileDevice: function(){
    if(Darwin.device.type === "phone" || Darwin.device.type === "tablet"){
      return true;
    }else{
      return false;
    }
  },
  readyFoto: function(){
    if(Session.get('userInfo') && Session.get('name') && Session.get('desc') && Session.get('price') && Session.get('pais') && Session.get('estado') && Session.get('ciudad') && Session.get('catName') && Session.get('foto1') && Session.get('foto2') && Session.get('foto3') && Session.get('foto4')){
      Router.go('/vender/publicar');
      return true;
    }
  }
});