Template.loading.rendered = function () {
  if ( ! Session.get('loadingSplash') ) {
    var message = '<p class="loading-message">La Aplicación está cargando los datos</p>';
    var spinner = '<div class="sk-spinner sk-spinner-rotating-plane"></div>';
    this.loading = window.pleaseWait({
      logo: 'logotipo.png',
      backgroundColor: '#ecf0f1',
      loadingHtml: '<p class="loading-message">La Aplicación está cargando los datos</p>' + '<div class="sk-spinner sk-spinner-rotating-plane"></div>'
    });
    Session.set('loadingSplash', true); // just show loading splash once
  }
};

Template.loading.destroyed = function () {
  if ( this.loading ) {
    this.loading.finish();
  }
};

