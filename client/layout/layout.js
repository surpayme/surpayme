Template.layout.transition = function() {
	return function(from, to, element) {
  		return 'iron-router';
	}
};

Template.layout.rendered = function(){
  Meta.setTitle("surpayME");

  Meta.set({
      name: "bitcoin",
      content: "bitcoin"
  });

  Meta.set({
    name: "ventas",
    content: "venta de cualquier producto o servicio"
  });

  Meta.set({
    name: "compras",
    content: "compra de cualquier producto o servicio"
  });

  Meta.set({
    name: "escrow",
    content: "usa el servicio de compra y venta con btc con escrow más confiable de venezuela"
  });

  Meta.set({
    name: "btc",
    content: "usa las cryptodivisas como medio de compra"
  });
};

Template.layout.helpers({

});