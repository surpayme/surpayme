Template.navbar.events({
	'click #loguearse': function(){
		return Router.go('/login');
	},
	'click #registrarse': function(){
		return Router.go('/registrarse');
	},
	'click #volver': function(){
		return history.back();
	}
});

Template.navbar.helpers({
	'mobileDevice': function(){
		if(Darwin.device.type === "phone" || Darwin.device.type === "tablet"){
			return true;
		}else{
			return false;
		}
	},
	imagenPerfil: function(){
					  var url = Gravatar.imageUrl(Meteor.user().emails[0].address, {
					    size: 240,
					    default: 'mm'
					  });

					  return url;
					}
});