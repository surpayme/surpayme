Template.mainPad.helpers({
	productos: function(){
		return Products.find({}, {sort: { price: Session.get('sort')}, limit: Session.get('limit')});
	},
	'mobileDevice': function(){
    	if(Darwin.device.type === "phone" || Darwin.device.type === "tablet"){
      		return true;
    	}else{
      		return false;
    	}
    },
    cantidadLoad: function(){
		if(Products.find().count() <= Session.get('limit')){
			return false;
		}else{
			return true;
		}
  	}
});

Template.mainPad.events({
	'click #loadMore': function () {
		if(Session.get('limit') <= Products.find().count()){
			return Session.set('limit', Session.get('limit') + 3);
		}
	}
});